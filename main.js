let personOne = createNewUser();
let personTwo = createNewUser();

function createNewUser() {
    const newUser = {};
    newUser.name = prompt("Enter your first name: ");
    newUser.lastname = prompt("Enter your last name: ");
    newUser.birthday = prompt("Enter your birthday dd.mm.yyyy: ");
    newUser.getLogin = function () {
        let login = newUser.name[0].toLowerCase() + newUser.lastname.toLowerCase();
        return login;
    };
    newUser.getPassword = function () {
        let year = newUser.birthday.slice(6);
        let password = newUser.name[0].toUpperCase() + newUser.lastname.toLowerCase() + year;
        return password;
    };
    newUser.getAge = function () {
        let currD = new Date();
    let currYear = currD.getFullYear();
    let currMonth = currD.getMonth();
    let currDate = currD.getDate();
    let bYear = newUser.birthday.slice(6);
    let bMonth = (newUser.birthday.slice(3, 5) - 1);
    let bDate = newUser.birthday.slice(0, 2);
    let age = currYear - bYear;
    if (currMonth < bMonth) {
        age = age - 1;
    }
    if ((currMonth === bMonth) && (currDate < bDate)) {
        age = age - 1;
    }
    return age;
    };
    return newUser;
}

console.log(personOne);
console.log(personTwo);